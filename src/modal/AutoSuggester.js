import React from 'react';
import Autosuggest from 'react-autosuggest';
import TextareaAutosize from 'react-textarea-autosize';

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

const dummyInput = [
  {
    word: 'Dogmatic',
  },
  {
    word: 'use',
  },
  {
    word: 'of',
  },
  {
    word: 'catapults',
  },
  {
    word: 'by',
  },
  {
    word: 'catatonic',
  },
  {
    word: 'operators',
  },
  {
    word: 'results',
  },
  {
    word: 'in',
  },
  {
    word: 'dire',
  },
  {
    word: 'consequences',
  },
];

var currentInput = [];

function getSuggestions(value) {
  const escapedValue = escapeRegexCharacters(value.trim());

  if (escapedValue === '') {
    return [];
  }

  const regex = new RegExp('^' + escapedValue, 'i');

  if (dummyInput === '') {
    return;
  }

  return currentInput ? currentInput.filter(currentInput => regex.test(currentInput.word)) : dummyInput.filter(dummyInput => regex.test(dummyInput.word));
}

function renderSuggestion(suggestion) {
  return <span>{suggestion.word}</span>;
}

function getSuggestionValue(suggestion) {
  return suggestion.word;
}

export class AutoSuggester extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bodyInput: '',
      value: '',
      suggestions: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  // Body input methods
  handleChange(event) {
    var dataInput = JSON.parse(JSON.stringify(event.target.value));
    this.setState({bodyInput: dataInput });
  }

  handleSubmit(event) {
    console.log('Content was submitted: ' + this.state.bodyInput);
    event.preventDefault();
    var directInput = this.state.bodyInput;
    var splitWords = directInput.split(' ');
    var output = [];
    for(var i in directInput) {
      output[i] = {"word" : [splitWords[i]]};
    }
    currentInput = output;
  }

  // Auto suggestion methods.
  onChange = (event, { newValue, method }) => {
    this.setState({
      value: newValue
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
   this.setState({
     suggestions: getSuggestions(value)
   });
  };

  onSuggestionsClearRequested = () => {
   this.setState({
     suggestions: []
   });
  };

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: 'Type something',
      value,
      onChange: this.onChange,
    };

    return(
      <div className="auto-suggestor-wrapper">
        <div className="body-wrapper">
          <form className="bodyInput" onSubmit={this.handleSubmit}>
            <label>
              <textarea type="text" value={this.state.bodyInput} onChange={this.handleChange} />
            </label>
            <input type="submit" value="Submit" />
          </form>
          <div className="outputWrapper">
            { this.state.bodyInput }
          </div>
        </div>
        <p>Once you have loaded your content this auto suggest will pick words from the dictionary.</p>
        <p>As there was no time left to implement the lookup based off the input above, dummy data has been added. Try searching for 'Dog' for example</p>
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          inputProps={inputProps}
        />
      </div>
    );
  }
}
