import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';

import TextareaAutosize from 'react-textarea-autosize';

// Auto suggestion.
import { AutoSuggester } from './modal/AutoSuggester';

class App extends Component {
  render() {
    return (
      <div className="auto-suggester">
        <header className="header">
          <img src={logo} className="logo" alt="logo" />
          <h1 className="title">Auto word suggestor tool</h1>
        </header>
        <p className="intro">
          Upload your text to build the dictionary from.
        </p>
        <AutoSuggester />
      </div>
    );
  }
}

export default App;
